---
title: The State of Home Audio Streaming
layout: page
---

# Players

## Subsonic/Libresonic/Airsonic

This group of home music libraries all initially spawned from the
previously open source Subsonic. When that became close source
Libresonic was forked from it, and when that was abandoned Airsonic
was forked.

Each of these servers use the same API, meaning any app that works for
one will work for the rest. This unfortunately has more downsides that
upsides,

- The Airsonic team will not implement any features that go beyond
  what Subsonic enables via API.
- The API is a security nightmare, and Airsonic won't fix it without
  Subsonic also fixing it. On every API call plaintext username and
  password are sent in the URL string.
  
**Subsonic/Libresonic/Airsonic must not be used on unsecured public networks!**

The mobile app situation is also a bit dire. The main app that is
recommended to users on android:
[Dsub](https://play.google.com/store/apps/details?id=github.daneren2005.dsub&hl=en)
is effectively, but not completely, abandoned. It's using very old
versions of Chromecast libraries and abandoned DLNA libraries. I
expect it to stop working at some point in the future and it's not a
small piece of work to get it working again.

I started my own fork [Xsub](https://github.com/vrih/XSub) but I'm not
sure I'm going to continue this when the server has inherent and
unavoidable security issues.

## Plex

[Plex](https://plex.tv) used to be the goto choice for home NAS's. You
can't argue that it does the core job of libray management
well. Despite being closed source, the fact that they're trying to
monetize through subscription gives me some hope that they have a good
incentive for continuing development and maintenance.

There has been a lot of backlash recently though. They seem to not be
satisfied with the offline home server product and are chasing more
online streaming content; you can stream straight from
[Tidal](https://tidal.com) and they are doing deals with various web
TV shows. This isn't necessarily a terrible thing, except they're
putting this in front of a users owned assets. From forum posts you
would believe that the Roku app is completely unusable.

## Emby

[Emby](https://emby.media) appears to be where a lot of Plex users are
moving to. I've just paid for a Plex pass so haven't got round to
trying Emby. It used to be a less featured Plex, but now it seems it
has caught up on the local features, without going too much into
online features. 

# A brief word on streaming protocols

All of the above applications in some way support both DLNA and
through mobile apps Google Cast & airplay. Of these protocols there is
only one that I consider *good* for home streaming - Google Cast.

## DLNA

In theory this is an open and standardised protocol. It does most of
what people would want from streaming. Unfortunately, it's poorly
implemented, there are tons of compatbility issues between devices,
and it's all but abandoned. The main android library for interfacing
with DLNA devices is abandoned and there are very few new hifi
components supporting it.

## Airplay

Very much an Apple only protocol, although there are ways of getting
it working elsewhere. The main downside is that it downsamples audio
to 192kbps.

## Cast

You're still locked to a mobile device with Cast or chrome browser,
but outside of that it's a pretty solid protocol that handles
supported media in its original quality. I would like to see this be
opened up a bit more so desktop apps could implement it, but having to
use mobile apps isn't too much of a hardship right now.
