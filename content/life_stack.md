---
title: Life Stack
layout: page
---

# Computing

- Macbook Air (2021) - M1
- iPhone 13

# Productivity

- [Neovim](https://neovim.io)
- [Things 3](https://culturedcode.com/things/)
- [Fastmail](https://www.fastmail.com)

# News

- [The Guardian](https://www.theguardian.com/uk)
- [The FT](https://www.ft.com/)
- [Wired](https://www.wired.com/)

# Home server

- Synology Diskstation
- [Tailscale](https://tailscale.com)
- [Jellyfin](https://jellyfin.org)
- [homeassistant.io](https://www.home-assistant.io/)
