---
layout: post
title: The dangers of a monoglot platform
slug: dangers-monoglot-platform
date: 2023-12-03
description: The article argues against the idea of having a monoglot platform, where all the software is written in the same language. While there are some clear benefits such as simplified hiring, easy security auditing and defining technology roadmap, these advantages can equally be reframed as a disadvantage. For instance, hiring for a set of skills can make you miss talent, and having a monoglot platform can rapidly become a security nightmare. Additionally, focusing on one language means you might fall behind on innovation, and a healthy company is a diverse company.
---

Many CTOs dream of the perfect platform where all the software is written in the same language, uses the same tooling and is deployed on a single consistent platform. There are some clear advantages to this: hiring is simplified because you can hire for a particular set of skills, it's easy to define security requirements and audit for compliance, and defining the technology roadmap becomes simpler. If you only use Java, you only need to be interested in what's coming in its future and can disregard the rest of the tech universe.

However, each of those advantages can equally be reframed as a disadvantage.

## Hiring for a set of skills can make you miss talent

When you know precisely the set of skills you're hiring for, it can be very tempting to narrow your search for employees to those that exactly match your requirements. This could be problematic depending on precisely what work you're hiring for. Consider the following scenarios:

* a Java engineer is developing an API and, knowing only Java, prioritises writing an API that works perfectly with Java type safety. However, the consumers of the API are likely to be using Javascript or Python, and the changes to make type safety easier have suddenly made the customer's lives more difficult.
* Engineers who tick all the boxes on requirements might not have kept up to date on their skills in ways that don't show during a testing process. They may be well-versed in the language and its tooling, but if they haven't kept up with the latest trends and technologies, they might not deliver the best solutions for the organisation's needs.

Don't get me wrong, there are plenty of great Java engineers who are up-to-date in their skills and knocking out great, innovative work. Still, most of the highest-value engineers I've worked with have regularly been productive across multiple languages. They could generally walk into a job in any language and be proficient and idiomatic within a month or two, uplifting the code with insights they're bringing from across the ecosystem.

## Having a monoglot platform can rapidly become a security nightmare

Whilst having everything consistent can make it easy to check for any security issues, when there is a significant issue, it can suddenly blow up into an event that disrupts the entire company roadmap for months. 

A great example is the log4j vulnerability at the end of 2021. I saw two very different patterns of responses to this event. Companies I would describe as polyglot had a much easier time as only a fraction of teams were impacted. Those teams could pause work to remediate, while security engineers were also dispatched to minimise interruptions. As a whole, the businesses saw little innovation impact. On the other hand, Java monoglot businesses were in full crisis mode. All teams had to stop innovation to remediate immediately, and the scale of the issue was so broad that tactical intervention by security engineers had minimal impact.

On a smaller scale, I see this on a weekly basis where the whole organisation needs to bump dependencies for Snyk to continue approving builds as new CVEs are released. It's a continuous cycle of hiccups across the roadmap.

## Focusing on one language means you might fall behind on innovation

By choosing to be monoglot, you're choosing to play in a particular sandpit and miss out on things that could be improving your life outside of it. If you were committed to being a pure Python company but suddenly had a service that needed to handle high volumes of web traffic, you'll be paying a high cost to scale that out rather than rewriting some services in a language like Golang that specialises in it.

On a similar theme, Java still lacks the equivalent of Goroutines (Golang) or Coroutines (Kotlin). This means that many Java organisations will experiment with Kotlin for very high concurrency. However, as they are ostensibly monoglot businesses, this is done half-arsed. The Kotlin code that comes out is not idiomatic and they can't move engineers around the organisation to work effectively on those projects because it's not culturally congruent. 

Ultimately, this costs a significant amount in infrastructure as a side effect. Image the monoglot Java company that has decided to architect microservices on Kubernetes and has 50 services, with many replicas, all running their own JVM. Just the weight of the JVM could easily make RAM requirements six times that of a Golang. Potentially, that could mean six times the infrastructure cost.

## A healthy company is a diverse company

Using a range of programming languages helps surface new ideas and approaches and builds empathy with customers who might have different tooling. It's the same reason that companies need diversity in the types of people they hire. Enabling polyglotism could potentially be a factor that helps you avoid an extinction event.Í
