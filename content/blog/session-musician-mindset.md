---
title: "Session Musician Mindset"
date: 2023-03-26T16:29:47+01:00
---
Mike Crittenden recently published a blog post, [Bandmates vs. session
musicians](https://critter.blog/2023/03/16/bandmates-vs-session-musicians/)
that really resonated, not only because I definitely fall into the session
musician camp as a technologist, but I also did as a musician.

My consultancy super power is being able to turn up in any scenario and be able
to understand the domain faster than most people. I also leave my ego very much
at the door. I turn up, try to understand what the client is after, and then
selectively pull things out of my toolbag that will get the job done in the
best way. Throughout the process I bring the experience of working on a number
of different 'sessions' to be able to guide the client towards approaches that
I've seen work before.

Likewise, as a musician, most of the work I did was musical theater/show and
function band work. In these scenarios you're very much hired for being able to
turn up, do a job professionally and not upstage the client.

The thing about the session mindset is that it thrives on novelty. As soon as
the work starts to become routine I'm bored and heading out to find something
different. The only way I've ever really stuck around longer term is where I've
been very much in a leadership position and been able to spread myself across
multiple projects to maintain variety.
