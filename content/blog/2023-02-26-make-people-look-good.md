---
layout: post
title: Make people look good
slug: make-people-look-good
date: 2023-02-26
categories: blog
---

A common theme in my career is that any progress I've ever made is simply from
making other people look good. At its simplest this is getting promotions
through getting my boss promoted - as a permanent employee there's a really
nice linear path to this.

In consulting it looks a little bit different. The aim isn't to directly get
promoted, it's extension of contracts and expansion of responsibilities.
There's also less direct potential for getting your boss promoted, nor is that
necessarily a good thing for securing future work, particularly in large
enterprises. Here the aim is to give your boss an easy life by taking over
their workload and making sure they don't have to direct you. As a side effect,
it also allows you to more easily steer what a workload looks like, albeit with
the risk that if the wheels start to come off then you're completely on the
hook for the consequences.


