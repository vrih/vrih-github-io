---
title: "Git should not be a mystery"
date: 2024-03-10T19:56:06+08:00
tags: ["blog"]
categories: [""]
draft: false
---

The number of senior developers who claim not to understand Git is worrying. If I had one recommendation for a junior developer, it would be to ensure they knew how to add, commit, rebase, fixup and squash from the command line. The git log is your portfolio on any project, and you should have the skills to make it look professional.

One of the first things I do when I get an offer for an internal developer transfer is to look at their commit history across the organisation. If I see a nice, long history of well-thought-out commits that each tackles one thing at a time, then I can be reasonably confident they write good code without actually reviewing a line of it. If I see a bunch of commits along the lines of 'Fixed typo' or 'Make tests pass', then I'll start to be concerned.

There are some great resources for git, most recently blog posts from [Julia Evans](https://jvns.ca/categories/git/) and [this video from GitButler](https://youtube.com/watch?v=aolI_Rz0ZqY). At this point, there's little excuse.

