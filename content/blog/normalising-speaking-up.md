---
title: "Breaking the Silence: Normalizing Speaking Up in Organizations"
date: 2024-03-24T17:19:00+08:00
tags: [""]
categories: [""]
draft: false
---

Organisations are generally riddled with social rules that evolve over time. As much as HR would want to show the company's culture through fun events and having a beer fridge, it's these unspoken social rules that end up defining the culture.

These social rules take several forms but they are often represented with hierarchies that exist beyond the org chart. For example, even though technology and sales may look equal on an org chart, there is often an unwritten rule not to question anything sales because nobody in tech wants to be able to be blamed for a sale not happening.

This leads to the all-to-frequent regrettable situation where a technology team might spend months developing features that they know are wrong and could even contribute to losing a sales pitch, but by not speaking up about it, they have stayed out of the blast radius.

The same behaviour happens within teams, where junior team members are too deferential to their seniors and don't question wrong decisions. Outside of technology, this can have disastrous consequences, as in [Korean Air Flight 801](https://en.wikipedia.org/wiki/Korean_Air_Flight_801).

## Lack of clear ownership prevents others from challenging

One of the biggest impediments to voicing disagreement is not having someone who has clear ownership over the area of disagreement. The fear is that if I disagree with you and the project direction is changed towards my opinion, it is now my responsibility. That shouldn't be the case. Disagreements should be logged, but it is ultimately up to the person responsible for deciding whether to maintain the current path or change the tack.

I have seen very senior engineers and product owners ultimately cave on their principles and not speak up because they didn't want to get the blame.

## There should be a robust process for disagreement

Disagreement, when not done correctly, can often become personal. It is also a very difficult thing to do, and lots of people are just not comfortable doing it face-to-face. For any decision, I always prefer to have some written artefact that feedback can be delivered on, whether that's a pull request for code, an architecture decision record, or even a project plan. It allows the disagreement to be about what's been written down instead of about personalities.

## Junior team members should be invited to disagree

Something I always look for in teams is how much junior team members review and critique the work of their seniors. At worst, they will come away having learnt something new, and maybe they will get the opportunity to feel great about themselves for spotting an error that somebody much more senior has skipped over. In fact, the junior team members often have more time to think a bit more deeply about the specifics of a problem. The challenge is making them feel like they have permission to challenge. The best thing to do is tackle that head-on and invite the challenge, explicitly asking individuals for feedback.
