---
title: "Children's Menus in the UK: Why Do They Always Disappoint?"
date: 2024-04-14T20:26:21+08:00
tags: [""]
categories: [""]
draft: false
---

![](/images/childrens-food.png)

Why is it that every restaurant in the UK does children's food badly? It's pretty much a universal truth that anywhere that lists a children's menu, whatever the culinary level of the restaurant, the food will be boring and also cooked badly.

Last week I took Little Man for his first [Nando's](https://www.nandos.co.uk/). My natural inclination was just to get him an adult dish, which is what we normally do. He would rather have something interesting than fish fingers, bad pizza and pasta with tomato sauce. The kid's menu at Nando's looked more reasonable, and at half the price, I thought we would try for it.

The vegetables and rice were well done, but they were exactly what would be served to an adult. What wasn't good was the chicken. Where I had been served a lovely juicy chicken thigh, little man got a thin piece of, inevitably, overcooked chicken breast. I'm not entirely sure what the reasoning is. Do they expect children to only like chicken breasts and not thighs? No child would have liked that breast, but a piece of chicken thigh that thin might have stood a chance.

I'm going to have to go back to my policy of just stumping up for an extra adult dish for Little Man. To be fair, most of the time he finishes it anyway.
