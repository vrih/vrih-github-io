---
title: "Software Development needs Apprenticeships"
date: 2024-02-26T20:02:32+08:00
tags: ["blog"]
categories: [""]
draft: false
---

Great software engineers are craftsmen, not scientists. There is often a perception that it is an academic pursuit and that the best candidates will have good Computer Science degrees from renowned universities. It's bizarre how this misconception persists, as anyone who has spent any time working with people in the software development and DevOps space will have observed these facts:

1. There are a lot of very mediocre colleagues with Computer Science degrees. They often have very outdated knowledge and implement the same patterns they learned 10 years ago.
2. There are a lot of very good colleagues who do not have Computer Science degrees. These will often be the most motivated people on the team and know how to get new information. They are always trying new things.
3. Brand new engineers out of university with good degrees require as much babysitting as those new to the industry without degrees.

The fact is, knowing how to code is very different to being able to produce something that is useful in a business context. There's also a lot of flexibility in the definition of the right way to code and every engineer needs to develop their own style through experience.

Instead of spending several years and tens of thousands of pounds on a degree, that time would be much better spent working as an apprentice for an experienced developer. It makes sense from the other side of the equation as well; every senior developer has mundane tasks that they could pass off to an apprentice in exchange for access to new work that will develop their skills further.

The challenge in the UK, at least, is that the concept of apprenticeships has been stolen by the Government and is purely a way to enable cheap labour with the bare minimum of training, with some inevitable funnelling of money to their friends. An apprenticeship isn't a work/study program with a meaningless qualification at the end of it, it's a period of study under a master until you develop the skills to be a master yourself.

## Do real apprenticeships exist?

I think they do, but you would be hard-pressed to find them. To narrow it down it's helpful to look at what isn't an apprenticeship:

- Pretty much anything explicitly advertised as an apprenticeship. These are mostly tied in with education schemes and are designed to make statistics showing people are in education, as well as providing cheap labour.
- Anything in a large organisation. Your apprenticeship is not tied to a master, and as such, it will be inconsistent, and you may not have anyone with experience to train you. You will be given tasks that are above your ability level and just expected to do them, and when you start failing there will be no support.

Your best chance is just getting a Junior Engineer role at a very small software development consultancy, the kind of place that you could stay for 10 years and the company isn't going to massively grow, but you will have the space and time to learn your craft while doing interesting work.
