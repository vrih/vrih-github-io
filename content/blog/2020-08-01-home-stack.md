---
layout: post
title: "Update on home automation setup"
slug: home-stack
date: 2020-08-01
---

Over the past couple of months, in preparation for some home renovation I've 
made some changes to my home automation setup.

## Zigbee for everything

Everything is moving to zigbee. Currently that means hue light bulbs, motion 
sensors, humidity sensors and door sensors. After renovation it will also be 
zigbee modules in the light switches. Most significantly I'll ditch the Nest 
thermostat and migrate to zigbee based.

## Node red

Home assistant is good for home automation, but node red really takes it up to 
another level.

## Roomba vacuum cleaner

I've seen home automation blogs say that robot vacuum cleaners were essential 
but I've never really thought it made sense for me. However, with a baby 
meaning we need to vacuum most days I thought I would give it a go. At £80 
secondhand on eBay it's pretty much a no brainer. There's definitely no going 
back.
