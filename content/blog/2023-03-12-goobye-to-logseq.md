---
layout: post
title: Goodbye to Logseq
slug: goobye-to-logseq
date: 2023-03-12
---

In 2022 I went through a run of trying different note taking tools and broadly
settled on [Logseq](https://logseq.com). It's a journal based logging tool with
storage in markdown -- something that made me feel better about trying it than
other proprietary storage based tools.

However, I learned a big lesson here. Logseq may use markdown but the way it
structures documents and organises things it may as well be storing in
a proprietary format. The only benefit I found was being able to do version
control storage of the notes. The migration from Logseq to another tool took
days to complete, mostly fighting with its bizarre output.

The other issue with Logseq was just how unpolished the UI is. Even copying and
pasting data out of it only worked a fraction of the time. I'm tempted to blame
it on being an Electron based UI, and I'm sure that's part of the cause, but
others have shown that it's possible to create slick and responsive Electron
UIs.

For some people Logseq is going to be a great tool, and it worked well for me
on an interview based project last year, until I had to share the output notes
with other people.
