---
layout: post
title: Your platform is a product and your developers are your customers
slug: platform-as-a-product
date: 2020-02-20
---

An increasing number of businesses are creating platform teams to build a solid 
foundation on which their developers can work. For this to be successful it's 
necessary to treat the platform as a product. Here I explore what that means.

## What is a platform?

A platform can broadly be described as "APIs, environments, services and 
practices that allow developers to focus on delivering value." Really it's a 
collection of any tools, templates and processes that allows a developer to get 
on with developing and not have to worry about Ops.

## Why have a platform?


<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   viewBox="0 0 238.78667 93.926969"
   version="1.1"
   id="svg8"
   sodipodi:docname="platform.svg">
  <defs
     id="defs2" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="1.4736829"
     inkscape:cx="451.25039"
     inkscape:cy="140.70936"
     inkscape:document-units="mm"
     inkscape:current-layer="g4526-6"
     showgrid="false"
     inkscape:window-width="1916"
     inkscape:window-height="1044"
     inkscape:window-x="0"
     inkscape:window-y="16"
     inkscape:window-maximized="0"
     inkscape:snap-text-baseline="false"
     fit-margin-top="0"
     fit-margin-left="0"
     fit-margin-right="0"
     fit-margin-bottom="0" />
  <metadata
     id="metadata5">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(-8.3152545,-44.700949)">
    <g
       id="g4735"
       transform="translate(0,-0.1259323)">
      <g
         id="g4526"
         transform="matrix(1,0,0,0.86097547,0,6.6307939)">
        <path
           inkscape:connector-curvature="0"
           style="opacity:1;fill:#bbbbbb;fill-opacity:1;stroke-width:0.2838878"
           d="M 8.3152545,44.58322 V 153.36913 H 57.452307 V 44.58322 Z"
           id="rect4518" />
        <rect
           style="opacity:1;fill:#ee6d30;fill-opacity:1;stroke-width:0.26458332"
           id="rect4522"
           width="49.136833"
           height="13.985077"
           x="8.3154755"
           y="44.889877" />
      </g>
      <text
         id="text4574"
         y="54.475281"
         x="12.060175"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:sans-serif;-inkscape-font-specification:'sans-serif, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.26458332"
         xml:space="preserve"><tspan
           style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;font-family:sans-serif;-inkscape-font-specification:'sans-serif, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1"
           y="54.475281"
           x="12.060175"
           id="tspan4607"
           sodipodi:role="line">Traditional</tspan><tspan
           style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;font-family:sans-serif;-inkscape-font-specification:'sans-serif, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1"
           y="65.058617"
           x="12.060175"
           id="tspan4609"
           sodipodi:role="line" /></text>
      <text
         xml:space="preserve"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:center;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:middle;fill:#000000;fill-opacity:1;stroke:none"
         x="32.588192"
         y="80.151749"
         id="text1044"><tspan
           sodipodi:role="line"
           id="tspan1042"
           x="32.588192"
           y="80.151749">Product</tspan><tspan
           sodipodi:role="line"
           x="33.764347"
           y="90.735085"
           id="tspan1046"> Dev </tspan><tspan
           sodipodi:role="line"
           x="33.64756"
           y="101.31841"
           id="tspan1048">QA </tspan><tspan
           sodipodi:role="line"
           x="32.588192"
           y="111.90175"
           id="tspan1050"></tspan><tspan
           sodipodi:role="line"
           x="32.588192"
           y="122.48508"
           id="tspan1052">Ops</tspan></text>
    </g>
    <g
       id="g4748"
       transform="translate(-0.56698443)">
      <g
         id="g4526-3"
         transform="matrix(1,0,0,0.86341449,63.783519,6.1313665)">
        <path
           style="opacity:1;fill:#bbbbbb;fill-opacity:1;stroke-width:0.99999994"
           d="M 31.427734,222.51953 V 579.66211 H 217.14258 V 222.51953 Z"
           transform="scale(0.26458333)"
           id="rect4518-6"
           inkscape:connector-curvature="0" />
        <rect
           style="opacity:1;fill:#ee6d30;fill-opacity:1;stroke-width:0.26407596"
           id="rect4522-7"
           width="49.136833"
           height="13.985077"
           x="8.3154755"
           y="44.889877" />
      </g>
      <text
         id="text4578"
         y="53.066799"
         x="87.435936"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.26458332"
         xml:space="preserve"><tspan
           style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1;stroke-width:0.26458332"
           y="53.066799"
           x="87.435936"
           id="tspan4576"
           sodipodi:role="line">Agile</tspan></text>
      <text
         xml:space="preserve"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:center;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:middle;fill:#000000;fill-opacity:1;stroke:none"
         x="96.580482"
         y="90.735146"
         id="text1056"><tspan
           sodipodi:role="line"
           id="tspan1054"
           x="97.756638"
           y="90.735146">Dev </tspan><tspan
           sodipodi:role="line"
           x="96.580482"
           y="101.31848"
           id="tspan1058"></tspan><tspan
           sodipodi:role="line"
           x="96.580482"
           y="111.90181"
           id="tspan1060">Ops</tspan></text>
    </g>
    <g
       id="g4760"
       transform="translate(-1.4174109,-0.06299197)">
      <g
         id="g4526-6"
         transform="matrix(1,0,0,0.86341449,127.85048,6.1313664)">
        <path
           style="opacity:1;fill:#bbbbbb;fill-opacity:1;stroke-width:0.28348655"
           d="M 8.3152545,44.962817 V 153.44142 H 57.452307 V 44.962817 Z"
           id="rect4518-2"
           inkscape:connector-curvature="0" />
        <rect
           style="opacity:1;fill:#ee6d30;fill-opacity:1;stroke-width:0.26458332"
           id="rect4522-9"
           width="49.136833"
           height="13.985077"
           x="8.3154755"
           y="44.889877" />
      </g>
      <text
         id="text4582"
         y="53.116409"
         x="146.21742"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.26458332"
         xml:space="preserve"><tspan
           style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1;stroke-width:0.26458332"
           y="53.116409"
           x="146.21742"
           id="tspan4580"
           sodipodi:role="line">Devops</tspan></text>
      <text
         xml:space="preserve"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:center;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:middle;fill:#000000;fill-opacity:1;stroke:none"
         x="160.45728"
         y="102.11016"
         id="text1064"><tspan
           sodipodi:role="line"
           id="tspan1062"
           x="160.45728"
           y="102.11016">Dev</tspan></text>
    </g>
    <g
       id="g4773"
       transform="translate(2.5843911e-5)">
      <g
         id="g4526-5"
         transform="matrix(1,0,0,0.86341092,189.64959,6.1315103)">
        <path
           style="opacity:1;fill:#bbbbbb;fill-opacity:1;stroke-width:0.99999994"
           d="M 31.427734,222.51953 V 579.66211 H 217.14258 V 222.51953 Z"
           transform="scale(0.26458333)"
           id="rect4518-3"
           inkscape:connector-curvature="0" />
        <rect
           style="opacity:1;fill:#ee6d30;fill-opacity:1;stroke-width:0.26458332"
           id="rect4522-5"
           width="49.136833"
           height="13.985077"
           x="8.3154755"
           y="44.889877" />
      </g>
      <text
         id="text4586"
         y="53.959728"
         x="206.71843"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.26458332"
         xml:space="preserve"><tspan
           style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;writing-mode:lr-tb;text-anchor:start;fill:#ffffff;fill-opacity:1;stroke-width:0.26458332"
           y="53.959728"
           x="206.71843"
           id="tspan4584"
           sodipodi:role="line">Platform</tspan></text>
      <text
         xml:space="preserve"
         style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:8.46666622px;line-height:1.25;font-family:Arial;-inkscape-font-specification:'Arial, Normal';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:center;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:middle;fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.26458332"
         x="222.61398"
         y="90.54615"
         id="text1080"><tspan
           sodipodi:role="line"
           x="222.61398"
           y="90.54615"
           id="tspan1070"
           style="stroke-width:0.26458332"><tspan
             x="222.61398"
             y="90.54615"
             id="tspan1066"
             style="stroke-width:0.26458332">Dev</tspan><tspan
             dx="0"
             x="237.67043"
             y="90.54615"
             id="tspan1068"
             style="stroke-width:0.26458332" /></tspan><tspan
           sodipodi:role="line"
           x="222.61398"
           y="101.12949"
           id="tspan1074"
           style="stroke-width:0.26458332"><tspan
             x="230.1422"
             y="101.12949"
             id="tspan1072"
             style="stroke-width:0.26458332" /></tspan><tspan
           sodipodi:role="line"
           x="222.37834"
           y="111.71282"
           id="tspan1078"
           style="stroke-width:0.26458332"><tspan
             x="222.37834"
             y="111.71282"
             id="tspan1076"
             style="stroke-width:0.26458332">Ops</tspan></tspan></text>
    </g>
  </g>
</svg>

### Traditional

The traditional way of running a technology team would be to have separate 
teams for each discipline. 

- Product managers are in their ivory towers making product decisions completely separate from any of your other teams.
- Developers are writing code to the product specs they receive, without really caring why.
- QA receive untested code from the developers and check it meets specs.
- Ops respond to ticket requests for environments and infrastructure changes, like opening firewall ports.

At first glance it seems like this is a nice separation of responsibilities, 
but in reality it's a disaster. None of these teams is really working in a 
silo; every action they take is likely to impact one of the other teams. A 
delay in Ops is going to directly translate to a delay in every other team. You 
have **"coupled backlogs"**.

### Agile

The most successful attempt to solve for coupled backlogs was Agile. This 
brought Product, Dev and QA into the same teams, forcing them to work together 
on tasks. They now had uncoupled backlogs.

From a feature delivery perspective, one of the main problems with Agile is 
that Ops remains a separate team and the backlog coupling still exists. In 
fact, I have seen organisiations where, because they have also made Ops Agile 
and work in sprints the delay to developers has actually increased as requests 
might wait a week before entering Ops next sprint.

### Devops

Devops tries to solve the problem of coupled backlogs by moving all operations 
work into the dev team. In theory that sounds great but the execution is 
normally sub-optimal. Instead of moving operations people into the dev team 
typically just the work is moved. This means you have developers now doing ops 
work, which is a completely separate speciality. Some developers are good at it 
and some become a liability as they unknowingly introduce security holes in the 
infrastructure as they only really care about getting their apps working.

The other problem with devops is how the value being provided by developers 
breaks down. Instead of spending half a day writing some code and moving on to 
the next feature they often find themselves spending an additional 2 days 
trying to get the infrastructure working.

### Platform

At first glance platform appears to be a regression towards Agile. Ops is once 
again a speciality all on its own. The key difference is that if you have got 
the platform right then the dev team never need to directly interact with the 
ops team. Specialised work is once again being done by specialists but with 
completely uncoupled backlogs.

In practice this will manifest as developers writing minimal config for the 
infrastructure, e.g. a YAML snippet defining required port openings, and that 
being automatically provisioned within pre-defined constraints.

## The mission statement

To help guide the direction of a platform it's useful to have a mission 
statement that describes what you are trying to achieve.

> A **stable and predictable** platform that is the developer's **preferred choice** 
> for deploying applications by **removing toil** and **reducing decision making**.

There are four key elements to this mission statement that really define what a platform should be.

1. **Stable and predictable** - Developers shouldn't be distracted by a platform 
   having poor availability and they shouldn't be consistently having to adjust 
   their integrations with the platform.
2. **Preferred choice** - Platforms only work best when developers aren't forced to 
   use them. The platform product manager should need to keep improving the 
   platform to make it so that the development teams choose to use it. There 
   may also be some use cases that as a product team you decide you don't want 
   to support and it's OK for developers to look elsewhere for a solution.
3. **Removing toil** - Certain tasks are trivial to automate and removing the 
   need for developers to handle them will increase their throughput and 
   accuracy. These may be tasks that are rarely done in any individual team but 
   across the organisation as a whole there's a lot of repetition, e.g. writing 
   deployments for JavaScript UIs.
4. **Reducing decision making** - Developers are frequently asked to make 
   decisions that sit outside of their expertise. *"What event bus should I 
   use?"*, *"How many partitions should my kafka topic have?"*. Anything that can 
   be done to move these questions to specialists is an organisational win.

## Ensuring individual features add value

While the mission statement gives a good overall direction there is a core set of criteria that all features should be assessed against.

- Is the feature self service for developers?
- Does it contribute to platform stability?
- Are difficult problems being simplified?
- Will the feature remove and standardise repetitive tasks?
- Does the feature avoid adding any constraints on developers?
- Are the number of decisions a developer needs to make reduced?
- Are new capabilities added without breaking backwards compatability?
- Does the feature provide measurable value (shortening development cycle/reducing infrastructure costs/adding security)?

The answer doesn't need to be *yes* to all of the questions but if it's 
*no* to any of the questions then there needs to be some other good 
justification for prioritising is as a feature.

## Discovering platform needs

### Pain point analysis

One of the simplest ways to discover what you should be adding to the platform 
is pain point analysis. This is as straight forward as surveying all of your 
developers to find out what they struggle with on the existing platform. These 
findings might conflict with metric based prioritisation, but one of the 
biggest challenges is gaining developer trust. The emotional response to the 
platform is perhaps the key success driver within an organisation.

This can be monitored with a variant of a net promoter score. Survey developers 
quarterly on "How likely would you choose this platform for your next project?" 
and look at the direction of the response scores. If they start going down then 
that's a big red flag for developers looking for other platforms.

### Developer workflow analysis

The key platform metric for the business is how fast are developers moving from 
ideation to delivery. The platform should be removing any steps that slow this 
process down. A product manager should be sitting with other product teams to 
analyse the journey from a developer starting on an issue and deploying. 
Measurements should be set up that track this duration, within the context of 
the type of feature being delivered.

### New developer on boarding flow

If it takes a new developer weeks to be able to get to a point of deploying on 
the platform then there is the potential that a huge amount of productivity is 
being lost. This is particularly critical in times of rapid scale up. The time 
from a new developer starting employment to deploying first application should 
be measured and ideally the product manager should be shadowing developers in 
each joining cohort.

### Platform availability

This is perhaps the most straightforward of the sources for features. The 
platform needs to be available so any feature that improves that, within the 
scope of defined SLOs, should be put into the backlog. The more difficult task 
is to understand when to prioritise more emotion based features ahead of metric 
driven availability feature. It's very easy to fall back onto a path where it's 
easy to measure the outcome, but what's the point in a highly available 
platform if nobody wants to use it.

## Success?

I've touched on some of the ways of measuring success above but success of the 
platform can be more broadly defined. Is it delivering value to the business? 
How value is defined is going to be slightly different for every business, but 
the one thing in common is that maximising return on investment from developers 
will be a key driver. If that is kept in mind then you're on the road to success.
