---
layout: post
title: My Apple productivity shortcuts
slug: mac-shortcuts
date: 2023-03-01
---

I'm very much into automating things as much as possible. That stretches
through to how I use my computers and at work and home I try to add shortcuts
to automate flows. Here are some of my key ones.

# Home

## Morning catchup

Every morning I grab my breakfast and go through the same list of apps and
websites to prepare for the day. I've simply got a shortcut that opens in
order:

* [Netnewswire](https://netnewswire.com)
* Social forums
* Email
* [YNAB](https://youneedabudget.com)
* [Things 3](https://culturedcode.com/things/)

After I've finished with each app I just ping back to the shortcuts window and
it opens the next. It means every day I can be sure that I've processed
everything I need right at the start of the day.

## Weekly processing

Similar to the morning catchup I have a shortcut that implements a similar flow
that opens apps to the content that I may need to organise for the week. This
is things like Download folders, articles starred to read, etc. Basically
anything that can be considered an inbox. This lets me keep things organised
and turn things into actionable items.

## Tracking purchases

I've got a simple shortcut that when I make an online purchase I run the
shortcut to add the item to Things 3 as 'to be delivered'. That helps me easily
keep track of if a delivery has gone missing.

## Apple text replacement

I find my email address annoying to type out. I've simply set it so typing
`@me` is replaced with my address. With the frequency that you need to type
your email address this is a pretty handy time saver.

# Work

## Daily standup

Using Alfred at work I have a simple shortcut that just opens up my Jira
backlog and starts the correct zoom meeting ready for me to start sharing. It's
an incredibly trivial action but as it's something I have to do every day it's
something work automating.

## Music

In my office I tend to listen to a set playlist. A simple shortcut on my phone
connects directly to my bluetooth speaker and starts the playlist in shuffle
mode. There's definitely more I want to do for automating my work environment
but for now that's still useful.

## Snippets

The snippets feature in Alfred is invaluable. Just having some ready to go
boilerplate for debugging some databases I've been working on has been
a massive timesaver as connecting via psql in a docker container in a remote
kubernetes cluster has zero chance of returning the right query when pressing
the up keyboard arrow.

## Apple text replacement

Again, super minor, but I'm responsible for approving my teams timesheets.
I can do this by simply replying to the timesheet email with the work APPROVED.
I found myself mistyping that so I've just added a simple replacement of zapp
to APPROVED to minimise the errors.
