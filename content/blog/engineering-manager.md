---
title: "Understanding the Multifaceted Role of an Engineering Manager"
date: 2024-04-01T19:59:44+08:00
tags: ["blog"]
categories: [""]
draft: false
---

![](/images/engineering-manager.png)

Some roles in technology are common to nearly all organisations of a certain size, but in each company, they can mean something completely different. If you're a software developer, what's expected of you will be essentially the same everywhere. However, once you make the leap to engineering manager, you suddenly get a lot of variety.

I believe a lot of companies get the engineering manager role completely wrong. It's easy to misread the job title as being a manager of engineers. Sure, that's part of the job, but you're not just responsible for managing the engineers but also for the engineering they produce.

## An engineering manager manages the engineers

The number one responsibility of an engineering manager is to ensure that the engineers working for them are happy and productive. They should know who the star players are, who the rock-solid devs are, and who needs more support. They should also be on top of how productive their team is being. The specifics of how to do this are worthy of a separate blog post, and there are as many strategies as there are managers. Between 1:1s, addressing performance issues, and just generally keeping an accurate temperature of the team, this is about 20% of the time spent.

## An engineering manager manages the projects

Within the team, the engineering manager should have ultimate accountability for the delivery of projects. It's their responsibility to understand the scope of projects in depth and the team's ability to execute against that scope. I've worked alongside many engineering managers who have lost control of this, often in scenarios where they are working with an outsourced team and aren't strong engineers themselves. For any conversation where the level of detail is higher than a single Jira ticket the manager should not need to go back to their team for more clarification -- it inevitably slows projects down.

That being said, it is acceptable (and necessary for career progression) for an engineering manager to delegate day-to-day responsibility for a project, as long as they remember that they will remain accountable and must stay on top of it.

## An engineering manager manages outwards to the organisation

One of the most critical roles for an engineering manager is to represent the team in the organisation. That involves handling a lot of the inbound communication from the org and filtering it down to what's relevant, as well as generating the proper outbound communication. This could be identifying future blockers and engaging other terms early to remove them or finding new opportunities for the team to make an impact. The engineering manager should understand how the organisation works better than anyone else on the team and be able to abstract that away.

## An engineering manager still writes code

The engineering manager role is the first rung on a career pathway that will eventually take the individual away from writing code professionally. They are much more likely to get involved in strategic and architectural decisions and unlikely to add new features directly. However, on the first rung, it is essential for an engineering manager to actively contribute code to be that effective link. They also need to be heavily involved in reviewing the code that is committed from all levels of the team to ensure they know what's being delivered and that it is of the right quality.

What will change is the type of code an engineering manager is writing. Instead of tackling the super-interesting problems that need to be completed by a deadline, they are much more likely to pick up technical debt, library upgrades, and small, well-scoped tasks. When a one-day task takes a week to deliver because of all the ancillary meetings, it's crucial for them to stay off the critical path, but they should also be able to drop all meetings and effectively contribute to getting the team out of shit if needed.

I have seen engineering managers who don't code any more and don't have a background in the same technologies that their teams are using. At that level, they are really acting as a pure people manager, and most of the time, they're largely dead weight on the team.
