---
layout: post
title: Google Chrome plans to violate your privacy
slug: chrome-privacy
date: 2019-08-24
---

In the latest [Google Chrome 
blogpost](https://www.blog.google/products/chrome/building-a-more-private-web/) 
Google outline a new project called Privacy Sandbox. They say this is intended 
to protect user's privacy, but in reality it is privacy harming and a betrayal 
of the role of what a web browser should be. Let me disect the main point 
they make.

## Privacy

### 1. Blocking cookies encourages bad behaviour

> First, large scale blocking of cookies undermine people’s privacy by encouraging opaque techniques such as fingerprinting.

Ignoring the fact that this is essentially victim blaming, it's also just plain 
wrong.

1. Browser fingerprinting is at least 10 years old at this point. The vast 
   majority of advertising companies do not use it, and wouldn't go near it 
   because it breaks most countries privacy regulations. It's just not worth 
   the risk of doing it and facing a GDPR fine.

2. Browsers that aren't Chrome actively block fingerprinting. Chrome is very 
   behind the curve on blocking that, and it's probably because of point 3.

3. Google are already one of the biggest users of fingerprinting technology. 
   Recaptcha uses finger printing techniques to try to identify legitimate users 
   vs bots. They're not explicit about this but compare the experience in 
   Chrome vs a browser like Firefox that blocks fingerprinting, where you'll 
   spend much of your time clicking on pictures of traffic lights or road 
   signs.

### 2. Blocking cookies threatens the future of the web

> Second, blocking cookies without another way to deliver relevant ads 
> significantly reduces publishers’ primary means of funding, which jeopardizes 
> the future of the vibrant web. Many publishers have been able to continue to 
> invest in freely accessible content because they can be confident that their 
> advertising will fund their costs. If this funding is cut, we are concerned 
> that we will see much less accessible content for everyone. Recent studies 
> have shown that when advertising is made less relevant by removing cookies, 
> funding for publishers falls by 52% on average.

Here Google is asserting a fact, and making it look legitimate by citing 
studies. They are being doubly disingenuous here. Firstly let's start with the 
claim that "blocking cookies significantly reduces publishers' primary means of 
funding". From a simple economic standpoint this argument makes no sense. 
If all cookies stopped existing it would likely have little impact on the total 
amount of money being spent on advertising - marketing still needs to happen. 
What will happen is the way the money is spent will change.

Firstly, advertisers will move back towards traditional methods of spending 
budget, finding the right context so that they're ads are relevant. The net 
impact is that site like the Washington Post will probably see an increase in 
spending, and average Joe's blog will see a drop. If anything the ROI for 
publishers will more accurately reflect the value of their content.

Secondly, they will fall back on other metrics to judge the performance of 
campaigns. Safari is already mostly untrackable to advertising companies, but 
there are very few advertisers that don't spend on it. They recognise that 
as a demographic these users are particularly valuable. Instead of tracking 
each individual user they track the cohort to determine ROI. Sure it seems less 
accurate, but cookie based tracking tends to over inflate the value of digital 
advertising anyway.

And that recent study Google quote - guess who performed that. Yes, Google. And 
the only conclusion you can make from their study that in a competition between 
cookied users and uncookied users the uncookied users are worth less. This 
doesn't tell you anything about a world where there are no cookies at all. 
The New York Times has [disabled ad 
exchanges](https://digiday.com/media/gumgumtest-new-york-times-gdpr-cut-off-ad-exchanges-europe-ad-revenue/) 
and seen revenues increase so there it seems there can be compatibility between 
privacy and advertising revenue.

So if Google's arguments are completely bogus, why are they making them. Well, 
the simple answer is if you replace the word publisher with Google. If nobody 
can track then things are a level playing field and Google lose revenue. If 
cookies continue to exist then Google can continue to leverage their other 
products to force people into being tracked and shut the door on smaller 
advertising companies.

## Conflict of interest

The biggest disappointment from this announcement is how Chrome's roadmap has 
been so directly influenced towards enhancing Google's advertising profits. Up 
until this point Google's incentive has been to use Chrome to improve the web 
with the knowledge that getting users a better experience, with faster page 
loads and greater reliability, would mean users spend more time on the web and 
increase the total revenue of the advertising market, thereby also increasing 
Google's revenue.

Between this move and last years incident where Chrome [didn't allow you to 
delete Google 
cookies](https://fossbytes.com/chrome-doesnt-delete-google-cookies/), as well 
as the disingenuous arguments made for crippling ad blockers in Chrome, there's 
been a big change to use the browser specifically as a competitive advantage for 
Google's advertising business. 

A web browser should be a piece of software that implements web standards to 
render content for the user. It should not be opinionated about that content 
and it should respect the wishes of the user - it is after all a piece of user 
installed software on the user's equipment. It is up to the content providers 
to follow standards and best practices on their side to ensure that their 
content works on all browsers, but a web browser may do much more than just 
render a publisher's content. Judging by the related [Chromium blog post](https://blog.chromium.org/2019/08/potential-uses-for-privacy-sandbox.html)
Google seem to have a different opinion, with all of their reasoning being 
publisher/revenue led, not user led.

The current system we have now is definitely flawed. Cookies were never meant 
to be available in a 3rd party context but eventually browsers adapted and we're 
currently in a world were a user can have near complete privacy. Google's 
proposed changes browsers should leak personal information by design, which is 
not OK.


