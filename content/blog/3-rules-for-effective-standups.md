---
layout: post
title: 3 rules for effective daily stand-ups
slug: effective-stand-up
date: 2023-11-21

---

You either love or hate the morning stand-up, and if you hate it you're probably doing it wrong. In the various companies I've worked with I've seen a number of patterns and anti patterns that are the difference between a team being effective vs not making any progress at all.

# Person based is better than task based

This is probably the biggest differentiator that I've seen. The traditional approach for stand-up is to go round each team member and essentially answer three questions:

> 1. What did I work on yesterday?
> 2. What am I working on today?	
> 3. What issues are blocking me?

[Taken from Atlassian](https://www.atlassian.com/agile/scrum/standups)

The fact is, this simple approach works. Everyone is held accountable for their actions and it's very obvious if you turn up to stand-up with no progress. This is what I call the person based stand-up.

The alternative is task based stand-up. Essentially, the Product Owner opens up the Jira board and asks for an update on each ticket in turn. I've never seen this approach recommended but I've seen it appear independently in multiple places.

There are 2 big problems with the task based stand-up. Firstly, you know whether you're interested in a specific task or not and people tune out if they're not interested. There's no active discussion because people are checking their emails, or just staring into space until their turn to speak. The other, bigger, problem is that it's very easy to hide. Because everyone's interaction is in short bursts it can go unnoticed for a while when your update on every ticket is just that it's blocked, or no progress. And because nobody else is really listening you stay blocked with no real incentive to fix anything. I don't think it's a coincidence that where I've seen this is in teams that have used a lot of third party engineers.

# Stand-ups aren't for reporting

A daily stand-up isn't a reporting meeting. It's not just to let your PO know if things are being delivered on schedule or not. Your lead and the PO can have a weekly meeting to keep that up to date.

Stand-up is a meeting where all participants should be constantly participating. I would expect a constant stream of small interruptions through the process. Things along the line of:

* I've encountered that issue before and this is how I resolved it.
* You won't be able to do that because the pipeline is broken.
* Can we pair on that issue because I would like to understand more about how it works?

All of those interruptions on their own help increase velocity and the active engagement keeps the whole team aware of what the priorities. When someone is off sick for a while it becomes almost zero effort for someone else to pick it up.

# Asynchronous stand-ups aren't stand-ups and don't work

I've worked with some highly distributed teams that used asynchronous stand-ups. Each engineer would post to slack every day their stand-up notes. This made some logical sense, with teams being in different timezones, speaking a range of different languages,  and fully remote. However, the end result was that the engineers never read or engaged with each other's report, and most of the time the lead and product owner wouldn't bother either. It was purely going through the motions and essentially just information in case the client asked questions about progress.

I'm sure there are teams that it can work for but the level of discipline required is huge. It's definitely not something you could do as a standard approach across a whole business.

# So how do I make it work?

Simple,

1. Go round each team member and get them to answer the 3 questions.
2. Make sure people interrupt whenever they hear something relevant.
3. Ensure everyone is participating at the same time. It doesn't matter if it's start of day for some and end of day for others but they must be actively communicating.
