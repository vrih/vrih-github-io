---
layout: post
title: On being tribeless
date: 2023-03-07
slug: on-being-tribeless
---

At the weekend, when I was on the Underground people watching, I came to the
realisation that I've never really been properly part of a tribe, except
perhaps as a teenager. By tribe I mean a group of people with similar interests
and a shared culture/language.

As a teenager I guess I was part of the heavy metal tribe -- dressed in black,
listening to Metallica. It wasn't a large tribe, nor a particularly close
tribe. If anything it was more a collection of people that didn't belong to
other tribes.

I do wonder whether not belonging to a tribe has been career limiting. As
a non-football supporting, non-skiing, non-recreational drug taking person
I definitely miss out on some opportunities that come out of networking.
However, it also means that I avoid tribal conflicts -- I can work with any
tribe because there aren't negative tribal associations like supporting the
wrong football team.

