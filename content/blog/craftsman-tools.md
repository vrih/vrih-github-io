---
title: "Customizing Your Tools: Applying Craftsmanship Principles to Your Digital Life"
date: 2024-04-08T15:24:34+08:00
tags: [""]
categories: [""]
draft: false
---
![](/images/craftsman.png)

Any good craftsman spends time making their tools work for themselves. This might be to make them better suited to a specific task or to fit the user better. If you're using a tool every day, then it makes sense to customise the handle so it becomes an extension of you instead of just the handle designed for the average person.

With computers, the same principle applies. If you're using something daily, make it work better for you. For me, this falls into a few buckets.

# Adding key mappings and macros

As a long-time Vim user, I appreciate being able to do everything through the keyboard. One of the big advantages of being keyboard first is that, generally, you can modify the mappings to meet your needs better. An obvious one is to map rerunning tests to a quick-to-execute shortcut to reduce the time spent iterating.

Using tools like [Keyboard Maestro](https://www.keyboardmaestro.com/), you can also add new shortcuts to applications on a Mac that the developer didn't ever intend for. One that I use quite frequently is in the recipe app [Paprika](https://www.paprikaapp.com/). There is no shortcut to replace the current grocery list with the ingredients required for next week's recipes, but a reasonably simple macro executes that for me.

# Customising the input method

The standard Qwerty keyboard is an imperfect input tool. Even at its best, it forces your hand into unnatural positions. Combine that with having to access modifier keys frequently, and it's a recipe for tendon pain.

For longer than I can remember I have replaced the Caps Lock key with Ctrl/Cmd. That brings a valuable key into a space where it's not going to cause anywhere near as much strain on my hands. But this can be taken further. I've recently discovered [home row mods](https://precondition.github.io/home-row-mods). These keep your fingers closer to the right position and eliminate nearly all stretching. They take a little getting used to but are also surprisingly natural.

These are the simplest tweaks that I regularly make and what makes sense will be different for everyone, and that's kind of the point. You need to make your tools your own. If someone jumps onto my computer to do some work they are going to struggle to find my tools comfortable, but they supercharge what I can do.

