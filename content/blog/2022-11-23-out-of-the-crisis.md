---
layout: post
title: W. Edwards Deming - Out of the Crisis
date: 2022-11-23
slug: out-of-the-crisis
---

{{< figure height="300px" src="out-of-the-crisis/out-of-the-crisis.gif" >}}

It doesn't matter if you're in manufacturing, running a restaurant or
developing software, one of the biggest boosts to any business is in reducing
rework. The output is better quality, employees are happier and removing rework
cycles increases throughput.

In software delivery this is conceptually a solved problem - have cross
functional teams building software following the principles of CD and the
barrier to entry is low. Yet this is still not the default for many
organisations, both enterprise and startup.

We're nearly 40 years after the publication of Out of the Crisis, which on page
1 starts with the observation that producing higher quality work increases
productivity. Deming has been talking about this since the 1950s. Accelerate
(2018) backed this up with solid survey analysis.

At this point, if engineers are regularly performing rework to meet compliance
or reliability goals it talks to ignorance of 70 year old knowledge at
a management level.
