---
layout: post
title: Life Stack Update
date: 2023-03-06
slug: life-stack-update
---

Here's a quick update to my life stack. Most of the shift comes from a switch
to the Apple ecosystem -- it's such a cliche but for me it's just easier to
work with  as I'm older and have less time. I've also changed up my
productivity approach.

# Computing

Still using the 2021 Macbook Air but now I've got an iPhone 13 to go with it.

# Productivity

I've ditched Taskwarrior. I really loved its prioritising of tasks and being
able to set dependencies, but the workaround I was doing for syncing it made it
fundamentally less useful. I've also made the clearer distinction between work
and personal tasks so my complexity needs are down. I've switched to [Things
3](https://culturedcode.com/things/).

I've also stopped trying to use any variant of mutt for email. It's a losing
battle against html email and Fastmail's client is about as good as one can be.

# Home server

There are a couple of small changes on the home server. Firstly I've swapped
from OpenVPN to [Tailscale](https://tailscale.com) as it's just more straight
forward. I've also dropped Syncthing; it's still one of the best sync tools
I've used but doesn't really work with iOS. Instead I'm largely relying to
iCloud and apps native sync interfaces to handle that.
