---
layout: post
title: Science Museum Review
slug: science-museum-review
date: 2023-03-05
---

Our second museum trip in 8 days and this is a big contrast from the last one.
Firstly, it was a much shorter trip, but also a much cheaper one.

Little man is still too young to do most of the paid for things at the [Science
Museum](https://www.sciencemuseum.org.uk). We stuck pretty much to the main
galleries. There wasn't that much to keep a toddler engaged -- once we'd seen
[Puffing Billy](https://en.wikipedia.org/wiki/Puffing_Billy_%28locomotive%29)
he was just about done and ready for the shop and food.

Of the toddler suitable things we went to the
[Pattern Pod](https://www.sciencemuseum.org.uk/see-and-do/pattern-pod) and the
[Garden](https://www.sciencemuseum.org.uk/see-and-do/garden).  Both of these
are essentially just play areas and I don't feel like we got much out of them.

It was fun introducing him to Pong, even if I did win 10-1, but we were really
done in 30 minutes. There's a reason the Natural History Museum has long queues
to get in at the weekend but the Science Museum was relatively quiet, it's just
not as engaging.

We'll make a trip back in a couple of years when he can do things like the Red
Arrows simulator and Wonderlab.
