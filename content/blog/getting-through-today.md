---
title: "What to do to make it through today"
date: 2023-03-24T07:17:41Z
---
I have one simple strategy for planning my work for the day; it's to answer
this question:

> What do I need to do today to be able to walk away with a sense of
> completeness?

That doesn't mean completing all tasks, but it does mean that every task should
get to a point where I feel that I've done enough and it can be picked up the
following week.

The key to this is maintaining a strong sense of deadlines and ensuring planned
work does not exceed time available -- something that is achievable but I only
realised post-30 years old.
