---
title: "Two Weeks with a Split Ortho-linear Keyboard: My Experience So Far"
date: 2024-04-28T14:45:36+08:00
tags: ["blog"]
categories: [""]
draft: false
---

![](/images/keyboard.png)

I'm two weeks into using a split ortho-linear keyboard, specifically the [ZSA Voyager](https://www.zsa.io/voyager). The move to a keyboard like this seems like an obvious decision; the horizontally staggered keys of a standard keyboard force your hands into some unnatural moves. I think it is one of the reasons I've never properly learned to touch type. After 2 weeks with the new keyboard, I've made a lot more progress than I ever had before. The split layout also massively helps with avoiding hunching over at the keyboard. The top half of my body is no longer curved inwards to get my hands right next to each other.

There are some downsides, although I suspect these might be short-lived. My typing speed has taken a bit of a hit. I have to engage my brain while typing, which both slows me down and tires me out. After 2 weeks, though, I reckon I've already closed the gap to my old speed by 50%, and with better discipline, I'm sure I'll end up a faster, more accurate typist. The other downside is I notice even more how uncomfortable typing on a laptop keyboard is. I guess you can't miss what you never knew. I need to work out some setup to use the keyboard comfortably in my lounge.

This is one of those areas where there is a better design, but due to standardisation, 99% of people will be stuck using the traditional style. Apple couldn't release a Macbook with an ortho-linear keyboard because very few people would buy it. The same could be said for key layouts. Something like [Colemak](https://en.wikipedia.org/wiki/Colemak) is probably much better to use, but standardisation means if you do learn it, you'll always be doing something unusual and not well-supported.

I'm going to persist with using the keyboard, if nothing else, because it adds a little extra interest to my environment. The only question is, when do I introduce my 4 year-old son to alternative keyboard layouts?
