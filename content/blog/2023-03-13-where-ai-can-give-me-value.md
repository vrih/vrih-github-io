---
layout: post
title: Where AI can give me value
slug: where-ai-can-give-me-value
date: 2023-03-13
---

With all the fuss that ChatGPT can generate large amounts of code that use case
is generally not that interesting to me. If ChatGPT is trying to solve it then
is it a novel challenge that you should even be writing code for in the first
place, or using libraries that tackle it better.

Where ChatGPT could have helped was a task I tackled last week. I'd coded up
a large data mapping exercise of a couple of hundred fields. Each field needed
a test to ensure that the mappings are correct. This feels like a task that
given a couple of examples AI should have been able to accomplish pretty
quickly, but I don't think the current generation of robotic parrots are quite
up to it.

