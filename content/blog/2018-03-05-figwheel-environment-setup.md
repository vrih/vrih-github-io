---
title: Setting up a dev environment for figwheel
layout: post
slug: figwheel-environment-setup
date: 2018-03-05
---

The standard figwheel template doesn't implement unit-testing and static code analysis out of the box, 2 things that I always need.  This is how I set up my figwheel dev environment from the basic template.

First I start with the `lein-figwheel` template.
    
    lein new figwheel <project-name> -- --reagent

This creates a simple shell of a reagent application. It's a good starting point, but we need testing.

## doo

The key plugin for testing is [doo](https://github.com/bensu/doo).

In `project.clj` add the following to plugins:

    [lein-doo "0.1.7"]

In the `:cljsbuild` section add something like the following:

``` clojure
{:id           "test"
 :source-paths ["src" "test"]
 :compiler     {:main            runners.doo
                :optimizations   :none
                :output-to       "resources/public/tests/all-tests.js"
                :pretty-print    true}}
``` 

You'll see that refers to a `:main` called `runners.doo`. Let's create that.

In `test/runners/doo.cljs` I put the following content (assume cljs-project is the name of the project):

``` clojure
(ns runners.doo
  (:require [doo.runner :refer-macros [doo-all-tests]]
            [cljs-project.test-core]))

(doo-all-tests #"cljs-project\..*-test")#
```

This tells doo where to look for the tests. Now you can write your tests as usual.

``` clojure 
(ns cljs-project.test-core
  (:require [cljs-project.core :as sut]
            [clojure.spec.test.alpha :as stest]
            [cljs.test :as t :include-macros true]))

(t/deftest test-plus
  (t/testing "1 + 1"
    (t/is (= (+ 1 1) 2))))
```

Doo relies on external programs to create the environment to run the tests. Headless chrome is probably the best option for this, especially now that phantomjs is abandoned.

Running tests is then as 

``` bash
lein doo chrome test
```

## Static analysis

I never leave home without kibit in my plugins section.

    [lein-kibit "0.1.5"]
    
It's a staic code analyser that picks up quite a few ways of making sure your code if more idiomatic, more readable and more efficient, e.g. if tempting to write `(= x 0)` to test whether `x` is 0, but the better way is `(zero? 0)`.

## Conclusion

From this point on there's not a whole lot more you need to do. For the development process, run `lein figwheel` and follow the instructions to navigate the browser to the local server. Code changes will be reflected almost instantly, allowing you to iterate very quickly.
