---
title: "The Invisible Leash: How Rigid Acceptance Criteria Stifle Developer Autonomy and Produce Worse Results"
date: 2024-10-29T20:41:47+08:00
tags: ["blog"]
categories: ["blog"]
draft: false
---

![](/images/acceptance_criteria.png)

There are many productivity dead ends contained within Agile implementations, and Acceptance Criteria (AC) are the worst.

Acceptance criteria are specific conditions or requirements that a project or task must meet to be considered complete and satisfactory. They are meant to be standards agreed upon between developers and stakeholders on what done looks like. Comprehensive acceptance criteria will list both functional and non-functional requirements, including updating documentation.

On the surface, that sounds pretty reasonable, but there are a whole bunch of shortcomings.

## 1. Accurate Acceptance Criteria are expensive to create

For ACs to fulfil the purpose of defining 'done', they must be comprehensive. They need to think about edge cases, things that shouldn't be allowed to happen, and all the integration testing that needs to happen.

For most stories, you could easily spend longer writing a full set of acceptance criteria than implementing a change. What you gain is a better chance of correctness, but you blow the budget while doing it. If you're launching a satellite into space, sure, you need binders full of acceptance criteria, but outside of an incredibly niche set of industries, it's not worth it.

## 2. When you write a definition of done, that is what you'll get and no more

If you give somebody a list of criteria by which you're going to decide whether they've completed their job, 99% of the time they'll go through that list 1-by-1, and when they get to the bottom, they're done. This is doubly true for any outsourced work.

Technically doesn't sound unreasonable, but I've already outlined above that the ACs will not be complete. By providing ACs, you guarantee that the work will be incomplete.

## 3. Developers lose autonomy

For many developers, ACs feel like a leash around their necks being tugged by the PO. They lose their autonomy to decide what else could be done to improve a story as they work on it. Developers are effectively turned from skilled craftsmen to factory workers churning out widgets. Developers in a well-functioning company want to feel like they are contributing to the company vision, and getting that right can be a force multiplier. 

## So what to do instead?

The alternative is straightforward but takes time to get right. Firstly, you need a team of developers that understand the vision - what are you building for in 6 months? They should understand the product well enough that you don't need a PO or BA to author most of the stories. They should also understand the standards within the organisation, such as how APIs are published, what documentation is expected, etc.

Once that is in place, stories can be high-level; the developer knows what is acceptable to complete that story and will go and do the ancillary work. Their reputation is now defined by the quality of what they choose to do to complete the story. As a result, the developers will also naturally level up. Autonomy breeds excellence.

Then, it's just left up to the team to review the work and decide whether that's acceptable according to the original vision.

So, ditch Acceptance Criteria and have a more high-performing team.

