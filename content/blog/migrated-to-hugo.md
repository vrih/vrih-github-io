---
title: "Migrated to Hugo"
date: 2023-03-15T15:06:08Z
---
I've been wanting to move away from [Jekyll](https://jekyllrb.com/) for
a while. It was frustrating that to even do a simple preview of posts that
I realistically had to run it in docker to ensure ruby versions and gem
versions all aligned. Sure it's easy enough, and it's kinda my day job, but
that's the point. I don't want the thing I quickly throw blog posts onto to
become as complicated as something I need to run for work.

[Hugo](https://gohugo.io) solves that frustration. It's a single Go binary
installable via brew and supported by most static site hosting services. It has
sensible defaults, but also was configurable enough to quickly match how the
Jekyll site was configured to avoid breaking too many links.

I've also taken the opportunity to return to using a pre-built theme. I'm
trying to remove temptation to fiddle too much and hopefully end up with
something that is minimal maintenance, even though that is kind of the fun bit
for me.

I kept Jekyll going for a few years, albeit with infrequent posts. Let's see
how long I last with Hugo.


