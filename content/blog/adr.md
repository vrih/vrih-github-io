---
title: "Architectural Decision Records: A Potent Tool for Consensus and Progress"
date: 2024-04-21T11:35:16+08:00
tags: [""]
categories: [""]
draft: false
---

![](/images/adr.png)

One thing I take with me from role to role is [Architectural Decision Records](https://adr.github.io). They are a potent tool in gaining consensus on engineering decisions and unblocking the way forward.

## ADRs in brief

An ADR is a document that records why a decision needs to be made, the context in which the decision is being made, the actual decision and any consequences. Typically, they'll be broken down into sections along those lines. They can be used simply as a record of any decision, but I get a lot more value when they're effectively used as an RFC, gaining input from a number of stakeholders and getting agreement on the approach.

There is an art to knowing when a decision needs an ADR. If a new record were entered every time a decision was made, that would slow things down. It should be used in scenarios where you can imagine someone asking you why you made that decision in the future.

## The worst thing is not to make a decision

The most significant power of ADRs comes in allowing a decision to be made and understood, with the understanding that a decision can be changed in the future. I've been in far too many meetings where nothing has been decided because people are searching for the perfect decision. ADRs allow you to make a decision and record the context in which the decision was made. If the context develops, you change your decision, but at least you have made progress. 

## Documenting as you go

ADRs stand as some of the best code documentation I have seen. They're written by engineers, generally with enough information to implement the decision, but also at a high enough level for less technical stakeholders to understand the change. When I onboard a new engineer to the team, one of the first places I send them is the current active ADRs.

## Preventing regret

Every engineer has felt regret at some code they have written. What they usually forget is that the code was written to meet a specific need, perhaps a certain scale or a different security context. The ADR records that context making it much easier to being understanding about code that was the right code for that previous context but now needs a different decision. It can also help with outward-facing conversations when explaining why something needs to be rewritten.
