---
title:  "New Year, New Blog!"
date: 2018-01-01
categories: admin
slug: new-year-new-blog
---

This blog is going to be a lot more general purpose than my previous attempts at blogging. With a bit of luck that will mean I might carry it on for a bit longer than the 2 to 3 posts that I normally do before giving up.

I'm not going to commit to anything in advance, other than a weekly update of things that I've been particularly working on.
