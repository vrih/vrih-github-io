---
layout: post
title: "This week - 2018-03-18"
date: 2018-03-18
slug: this-week
categories: week
---

This week I have been:

### personal computing 💻

- Fixed my wifi firmware, although I suspect I may have to reapply the fix on subsequent kernel upgrades.

### cooking 🍳

- Black Pepper Chicken
- Dinner at Lyle's by Sota Atsumi, formerly of Clown Bar in Paris. 

### Life ⽣

- Picked up the new car. 
- Got food poisoning, which knocked me out for most of the week.
