---
title: "Brio Brand Decay"
date: 2023-06-25T12:15:26+01:00
draft: false
---
My son loves his wooden railway set. He can play for it for hours at a time and
often ropes me into it. Over time he's come up with ideas for new layouts to
execute, which means paying £££ for expansion packs from Brio or going the
slightly cheaper route and getting Brio compatible set.

Loads of people do Brio compatible tracks --
[Ikea](https://www.ikea.com/us/en/p/lillabo-45-piece-train-set-with-track-20330066/)
and [John
Lewis](https://www.johnlewis.com/browse/baby-child/cars-trains-boats-planes/trains/_/N-fg2Z1z0ri3e)
are the ones I personally see most, but also things like Trackmaster can be
integrated with Brio sets. My assumption though would be that they were
charging less, had less brand equity, so were likely to be lower quality.

This brings me to my latest purchase a [Brio expansion set with buffers and
crossings](https://www.woodentoystore.co.uk/buy/brio-advanced-expansion-set-33307-wooden-railway-extra-track_110.htm).
This turned up and we got straight to building and it very quickly became clear
that something isn't right. A lot of the new track pieces just don't integrate
with our older Ikea and Brio pieces. You can sort of force it in, but I've
split a piece of track trying to separate already and there's no way my son can
do it on his own.

The only assumption I can make is Brio has engaged in the race to the bottom,
not necessarily on prices but on quality. The obvious comparison for me is
Lego; you can buy compatible bricks since their patent expired but everyone
knows that if you want the best fit you buy genuine. They then set out to
broaden their market with products appealing to broader age ranges. Admittedly
Brio would have struggled with the last part but I don't see a future in
a company that charges a premium but delivers quality much lower than their
cheaper competitors -- you're only ever going to buy Brio twice.
