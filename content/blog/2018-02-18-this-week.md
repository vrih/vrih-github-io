---
title: "This week - 2018-02-18"
date: 2018-02-04
slug: this-week
categories: week
---

This week I have been:

### personal computing 💻

- My [pull request for typeshed](https://github.com/python/typeshed/pull/1804) has finally been approved. There are definitely some things about python type handling that I don't like. The idea of typed python is great, but I suspect until there's another major version increment it'll always feel bolted on.

- Fixed my font preference order in Arch linux. I was getting some funky rendering of numbers in Firefox, but all is good now.


### cooking 🍳

- South Indian Chicken Biryani from [Maunika Gowardhan's Indian Kitchen](https://www.amazon.co.uk/Indian-Kitchen-Secrets-home-cooking/dp/1444794558/ref=sr_1_1?ie=UTF8&qid=1517152917&sr=8-1&keywords=indian+kitchen). My first Maunika failure - the rice ended up too sloppy and pretty bland flavour.

### Life ⽣

- Finally getting to the end of the flu. Peak fever was 39.2℃.

- Finished my full time role at Infectious Media. I'm staying on and doing a bit of contracting work for the next few weeks. I'm also looking for other new opportunities, as well as just taking a bit of a break.
