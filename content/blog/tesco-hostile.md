---
title: "Why is Tesco so hostile to shoppers?"
date: 2024-03-17T16:37:13+08:00
tags: ["blog"]
categories: [""]
draft: false
---

Online grocery shopping in the UK is a weirdly hostile experience, especially if you shop with [Tesco](https://www.tesco.com). The entire experience is built in an entirely opposite way to how a customer-centric site should be. The biggest pain points are:

1. You must search for items one by one. If you have a shopping list of multiple items, technically, there is a way to paste the list into the search, but it's useless and faster to do things one by one. Other sites like Ocado/Morrisons solved this problem in a much better way.
2. The site is dog slow. Navigating to an item can be measured in seconds. There's no excuse for this, and all other online grocery stores are much faster. It almost feels deliberate.
3. The checkout process is interminable. It's something like 7 steps of them trying to upsell you in the worst possible way. All of the stores do it; it's like having the sweets next to the till in the store, and sometimes I find it helpful. But Tesco's implementation is downright bad and exacerbated by their slowness. It's so bad that I get to the end of the process and sometimes fail to complete it, leaving items in my basket.
4. Every week, I get told my delivery will be late, even when it's early. Tesco has had some major outages recently, which I haven't been directly impacted by, but on a regular basis, there seems to be a complete disconnect between their order service and what their drivers are doing.

It just feels like either a deliberate move or the consequence of outsourcing website development to the cheapest bidder.  The other UK services may be afflicted by one of those issues, but the rest of the experience is generally fine. They're all theoretically easily solved problems, which means that the only deduction is that Tesco doesn't care about its online customers.

The good news is that for core groceries, most UK supermarkets are very close to each other in price, so it's relatively easy to just swap to another one. Goodbye, Tesco. Your drivers are great but not your website developers.
