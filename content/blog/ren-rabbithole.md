---
title: "Ren Rabbit Hole"
date: 2023-03-30T08:46:35+01:00
---
I've fallen down the [Ren](https://www.renmakesmusic.co.uk) rabbit hole. The
range of musical and poetic skills on display are just ridiculous and I don't
know anything else like him at the moment.

Top recommend is the [Tale of Jenny and
Screech](https://www.youtube.com/watch?v=TYAnqQ--KX0), a three part story with
such vivid imagery that it still triggers a strong emotional response on every
listen.
