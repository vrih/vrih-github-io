---
title: "Major Projects Poor Documentation"
date: 2023-03-21T08:46:46Z
---
In the past few weeks I've found myself increasingly struggling with poor
documentation from pretty significant tools that a lot of modern technology is
built on.

Firstly Spring Boot. This is problematic on 2 fronts. Firstly some of the
documentation is ambiguous; we've repeatedly had to confirm behaviours with
integration tests rather than rely on docs. The second problem here is the
official documentation is very rarely returned in Google. More often there'll
be stack overflow posts that comment on the behaviour with no reference to
where they got that information.

The second project is Elasticsearch. I defy anyone to look at
[Upsert](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-update.html#upserts)
and the Scripted Upsert docs and be able to explain exactly how it works. In
fact, previous versions of the documentation were more specific in their
language but also didn't reflect actual behaviour. The specified behaviour is
different enough that Opensearch and Elasticsearch actually seem to differ on
implementation. As an aside, Opensearch doesn't even document the scripted
upsert, even though it does appear to support it.
