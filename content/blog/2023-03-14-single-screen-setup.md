---
layout: post
title: My single screen setup
slug: single-screen-setup
date: 2023-03-14
---

Whilst many of my colleagues opt for running 2, 3 or even 4 screens while they
work I have spent the last 5 years on a single screen setup. In my opinion
having only one screen has made me more productive than 2 or 3.

# You can only focus on one thing at a time

I'm a big doubter of multi-tasking. Anything that looks like multi-tasking is
really rapid context switching. Your primary focus is always on a single task.
The question becomes how to most easily facilitate that rapid context switch.
With the 2 monitor setup the context switch is a head turn and refocusing of
the eyes. On a single monitor it's briefly popping up a window into your
existing frame of a view -- for me this is a much quicker task.

# You avoid the nagging feeling that something else is happening

If your second context is away and to the side there may be things happening in
that context that you feel like you could be missing out on. I find with
2 monitors I spend more time dipping out of my main context than in the single
monitor flow. I try to optimise so that anything important will pull my focus
on the single monitor and I can ignore anything else.

# There are use cases for dual monitors

I have to caveat everything I've put above with the context that I do very
little work that could be called UI programming. I can appreciate that if
you're working on a fullscreen design for an application you may need that
permanently available to you. But for everyday work, on a 16:9 screen there's
generally room for 2 side by side windows.

# The tooling makes a difference

One of the big reasons for multiple monitors is how badly Windows, and to
a lesser extent, MacOS handles window position. The paradigm of floating
windows is great for initial discoverability but breaks down when you need to
easily arrange things. The turning point for me was when I started using i3wm.
The ability to set applications per virtual desktop, essentially making them
all reachable with a single key-press, meant that I had the equivalent of 10
monitors. I'm still trying to replicate that flow on a Mac and
[Rectangle](https://rectangleapp.com) helps but I'm definitely missing that
single key press to always get the same app back.

