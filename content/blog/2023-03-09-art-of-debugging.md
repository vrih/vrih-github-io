---
layout: post
title: The art of debugging
slug: art-of-debugging
date: 2023-03-09
---

Debugging is something that I consider to be a particular skill of mine. Give
me something that isn't working correctly or needs to be rethought for
performance and I'll generally be able to get to a better solution. An incident
today made me slightly more aware of what the process is.

On moving some furniture I managed to completely knock out the internet in our
living room. With a deadline of a couple of hours before my son got back from
nursery and would want to watch TV I had to get it back working.

The symptoms were that the wifi was coming up but no internet was available,
except at one point there briefly was. There also didn't seem to be
a connection to the rest of the house network. I started going through the obvious
routes -- maybe a cable had been broken, reset the router to remove any factory
settings. Each time I would maybe get a glimpse of success before it failing
again, or make no progress.

The key observation I made about my behaviour was I would proceed down a track,
generate some learnings and then start the debugging process from scratch but
with added knowledge -- something like iterative context building. This process
of iteration stops me getting to attached to a single route and also allows me
to pick up other changing signals from the start of the process.

Having worked through the options it turns out that I had introduced a loop on
a switch. An easy fix in the end and a happy toddler when he got home from
nursery.

