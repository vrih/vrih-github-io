---
layout: post
title: Don't Move Improve Longlist
slug: dont-move-improve-longlists
date: 2023-03-08
---

We spent a year living out of our house while it was renovated and turned into
our dream house. While that was enough reward in itself we've also made it to
the longlist for [Don't Move, Improve!](https://dontmoveimprove.london) 2023 as [Breathable
House](https://dontmoveimprove.london/directory/breathable-house).



