---
layout: post
title: "The Great Space Race: Waltham Forest Hackathon "
date: 2019-05-06
slug: space-race
categories: hackathon
---

On May 1st and 2nd I took a team from Black Square Media to the
Waltham Forest ["The Great Space
Race"](https://walthamforest.gov.uk/content/great-space-race-hackathon)
hackathon. This was hosted in the new AWS building in Shoreditch. The
aim of the hackathon was to create a platform that would make it
easier for people to book council owned spaces in Waltham Forest.

We built our solution using our standard stack of [Grape
API](https://github.com/ruby-grape/grape) plus
[VueJS](https://vuejs.org)/[Vuetify](https://vuetifyjs.com)
frontend. By then dividing up the work, which VueJS makes easy to do,
we were able to produce a significant portion of a booking platform —
enough to win the first prize.

The hackathon was split into an Ideation phase and a build phase. The
council really committed resources to this hackathon and had a large
number of pretty senior people available to allow us to really deep
dive into what the problems were and how we might best be able to
solve them.

The biggest challenge of the hackathon was working out what was
feasible to build in the 24 hours available, and what would have to be
"next steps". Inevitably, the solutions started to converge on the
same idea, largely driven by what it's possible to deliver in 24
hours. Our key difference was really on picking up some of the
feedback from the community experts and ensuring they were key to our
design.

Hopefully this will lead to some next steps for the platform. It was
great engaging with my local council on this issue and it's definitely
a problem that needs solving.
