---
layout: post
title: The best note taking app
slug: best-note-taking-app
date: 2023-03-10
---

The best note taking app is the one that reduces friction on inserting notes.
At least, that's the conclusion I've come to after trying many different
approaches. Now I've largely gone in on Apple ecosystem it means for me, right
now, the best app is Apple notes. Here's why:

1. It's integrated with Apple search tools and there's Alfred workflows for it.
2. It syncs seamlessly between Apple devices.
3. The effort to input is minimal - there's aren't too many options to get
   started.
4. There are powerful features, like document scanning, for when you need them.
5. It's free.

That last point isn't a deal breaker. I'm happy to pay for a good app but the
trend seems to be for note taking apps to be subscription based. This is one
space where subscription makes no sense at all -- I don't need constantly
updating new features, I need stability.

The only concern I have with Notes is the lack of good export and how I prefer
to keep notes in Markdown format. That being said, I've tried note editors that
keep things in Markdown format and there isn't great compatibility between them
so not much is lost.


