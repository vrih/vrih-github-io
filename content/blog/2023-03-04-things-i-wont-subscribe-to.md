---
title: Things I won't 'buy'
slug: things-i-wont-subscribe-to
date: 2023-03-04
---

A story in 
[The Times](https://www.thetimes.co.uk) today about [Roald Dahl ebooks being
replaced with the new censored versions](https://www.thetimes.co.uk/article/roald-dahl-collection-books-changes-text-puffin-uk-2023-rm2622vl0)
in has reminded my that there are some
things I won't subscribe to any more. This hasn't always been the case, I used
to be a massive proponent of moving physical assets to digital forms. I don't
own any CDs or any means to play them. I've never had a big DVD collection.
I probably have more ebooks than physical books.

The problem with most digital assets is there is an illusion that the model is
ownership but in nearly every case it's rental, and often things are tied to
the method of consumption. My ebook collection, for example, is split between
Google Play Books and Kindle. I have to remember where the book is before
choosing the device to consume it on, and inevitably I give up before starting.

This has led me to the model that anything that I care about I will try to find
a way to properly own the content, roughly according to the following rules.

* I do not subscribe to a music service. I get Youtube music as a side effect of my 
  Youtube Premium subscription but it's garbage. Any music I'm going to play more than
  a few times I purchase a DRM free version. I'm old enough and own enough
  music that my need to purchase new music is quite limited and definitely
  cheaper than a Spotify subscription.
* Any books that I might want to read over a long period -- reference, popsci, 
  significant fiction -- I'll buy a physical, normally 2nd hand, copy of. If it's really light fiction then I might only get an ebook version.
* For videos I don't enter into any model that resembles ownership because it's incredibly rare for me to watch something multiple times and there is no DRM free format for me to purchase.
