---
title: "Hey Duggee Smut"
date: 2023-03-20T08:46:58Z
---
Hey Duggee has always been known for having plenty of references in it for the
adults but I think I may have found the most explicitly smutty one so far in
Season 4: The Library Badge:

```
NORI:       Hello old dears.
ROLY:       What you doing?
OLD DEAR 1: Nothing exciting.
OLD DEAR 2: It's all a bit dry at the moment.
ROLY:       Then read this!
OLD DEAR 1: Don't Spare the Sauce with Chef Buck Stag. Oooh, fully
            illustrated.
OLD DEAR 2: That should spice up our day.
SQUIRRELS:  Bon Appetito:
...
OLD DEAR 1: What does Chef Buck Stag tell us to do now?
OLD DEAR 2: Butter our dish.
OLD DEAR 1: Does he now.
OLD DEAR 2: All over.
OLD DEAR 1: By heck, that's going to taste good in't it.
```

No further comment on that one.
