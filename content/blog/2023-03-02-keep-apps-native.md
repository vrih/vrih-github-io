---
layout: post
title: Keep Apps Native
slug: keep-apps-native
date: 2023-03-02
---

There seems to be a continual march towards everything being a webapp but I'm
not on board. As far as possible I try to use native apps to whichever platform
I'm on and here's why.

# Quality is generally higher

This isn't really directly an issue on being native or not; there are some
great quality web apps, like [Fastmail](https://www.fastmail.com). However,
they are very much the exception that proves the rule. For most people offering
just a webapp experience then it often means they're doing it that way to save
money. That's not a great motivator for providing a great app experience.

# I can be in control of upgrades

The inevitability with webapps is that they undergo continual evolution. This
will inevitably break some element of my workflow. They may be chasing a new
product segment or just have a new product manager that wants to redesign and
make everything slower (looking at you here Gmail). If I'm happy with the
feature set on a native app I can just stick with the version I've got and
upgrade when I'm ready, although this may be API compatibility dependent in
some cases.

# Designed for power users

You only need to look at the difference between MS Office webapps and the
native equivalents to understand how productivity is generally a second class
citizen in the webapps. Sure you can get quick things done, but if you're
trying to create a complicated powerpoint presentation you're going to be
working much slower in the webapp. The app is a toy. Again, there's an
exception that proves the rule for Mac users here, and that is Outlook for mac
barely functions and the PWA is much more reliable and capable.


