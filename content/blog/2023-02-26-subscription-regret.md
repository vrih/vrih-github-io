---
layout: post
title: Subscription Regret
slug: subscription-regret
date: 2023-03-03
---

I'm constantly finding myself with subscription regret. So many apps have moved
to a subscription model and offer reasonable discounts for committing to a year
or longer. Inevitably, within about 10% of the subscription duration I find
a different service that I maybe would have preferred, but with the long lock
in I feel like I can't try it, or I regret spending money because a free
alternative would have done the trick. Top of my list right now are:

* [Inoreader](https://www.inoreader.com) - committed to 2 years on this but Netnewswire would do just fine for free.
* [YNAB](https://www.youneedabudget.com) - committed to 1 year, and it's a great app but [Moneywiz](https://wiz.money) looks like it matches my software philosophy better.
* [Pinboard](https://www.pinboard.in) - I committed to 3 years on this but it's starting to appear abandoned by the developer. There are better app based solutions for this - [Goodlinks](https://goodlinks.app) is one I'm looking at.

I don't have a good answer on how to avoid this other than only signing up
for monthly subscriptions. I suspect that I would be saving money in the
long run that way.
