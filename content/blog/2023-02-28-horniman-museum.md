---
layout: post
title: Horniman Museum Review
date: 2023-02-28
slug: horniman-museum
category: review
---

We went out for a father-son trip to the [Horniman
Museum](https://www.horniman.ac.uk/) last weekend. The primary aim was to go to
see the [Brick Dinos](https://www.horniman.ac.uk/event/brick-dinos/)
exhibition, which promised 20 models of dinosaurs and some time spent playing
with Lego. We ended up fitting in all of the museum and in general I don't
think it was value for money. You can definitely go and have a free day out and
only see the standard exhibits, but in theory the exciting things are what they
charge you for.

# Brick Dinos

We spent £13.50 on the promise of 20 dinosaur models. The advertising only
shows quite a large dinosaur so it was pretty disappointing when it turned out
there were only 4 or 5 large dinosaurs and the rest were small models that
really weren't impressive, and due to their scale and the size of Lego bricks,
not always that recognisable. We spent far longer playing with the Lego they
had out then looking at the models - it was really just an expensive play
session.

# David McKee

Little man recognised the pictures of Elmer so we decided to pop in to see this
exhibition. It only cost £7.50 but, other than some Elmer costumes they could
put on and some books to read there wasn't anything to really grab children's
attention. We were basically done in 5 minutes.

# Aquarium

Another £7.50 and possibly the best value of all the things we paid for. Litte
man was entranced by the clown fish and he could have stayed there for hours
talking to them. Some of the exhibits did seem conspicuously empty though.

# Butterfly house

From best value to worst - £14 to see about 5 butterflies. This is definitely
not something I would recommend to anyone. I can somewhat understand the
pricing just given the cost of the heating the butterfly house but the
experience was just so underwhelming and you don't get any closer to the
butterflies than you might in a park on a Summer's day.

As a museum that's meant to cater well for children it was frankly a bit
disappointing. Glad we went but won't be heading back any time too soon.
