---
title: "Embracing the Role of AI as an Editor and Marketer"
date: 2024-03-31T11:20:05+08:00
tags: ["blog"]
categories: [""]
draft: false
---

![](/images/ai-editor.png)

I feel like I'm a bit late to the AI game. Part of the reason is that we're not yet allowed to use it at work, although hopefully that's changing soon. It has started creeping into my personal life in subtle ways:

1. Rewrite recipes using simple steps. I already use [Paprika](https://www.paprikaapp.com/) to extract and store recipes. It does a great job of getting only the instructions and removing the life story. Still, with many recipes, those instructions can be way too wordy. Asking [Grammarly](https://www.grammarly.com) to rewrite the instructions as a set of steps for a professional chef gives me much more concise and actionable steps.
2. Suggesting blog post titles. I would never ask the AI to write the bulk of a blog post, but here it steps into the role of a newspaper editor suggesting a much more interesting headline than I would generally come up with.
3. Editing the content. Again, it's not something I ask Grammarly to rewrite, but I seriously consider it when it suggests clearer wording.

The best use I'm finding for AI is when a human has done the base creative work, and the AI then sits as an editor or marketer. Even then, I realise it's not as good as the best human at doing that, but in an environment where it makes no economic sense for me to hire a human, it does a good enough job.

