---
title: "More Home Automation"
date: 2023-03-23T15:27:07Z
---
I spent a bit of time off today refining some home automation. With the node
red setup I have adding any additional automations is super easy and means
I can get quality of life improvements for little effort.

The simpler piece I did today was digging out a smart switch and adding it to
my son's night light. That's now on a timer and controllable from home
assistant as well, which will resolve some mucking around we've been doing at
night to make him happy with light levels.

More challenging I added a discord bot to publish notifications into discord
and also accept simple commands. I don't really like home the home assistant
companion app deals with notifications, so it's easier if I send them into
discord, where I also have notifications from other services end up as well.
I've also made it so that I can simply trigger some scenes.

All in all, it was probably about 30 minutes work, including learning how to
setup a discord bot from scratch, which I think is a great return on time
investment.
