---
title: About
layout: single
url: /about/
---

Hi there! I am an Engineering Manager at [10x Banking](https://www.10xbanking.com), with a background as a consultant in cloud and data. My passion lies in exploring the intersection of privacy and automation, and I am constantly looking for innovative ways to balance the two.

## Certifications

### AWS
<style>
figure{margin:0}
</style>
<section style="display:flex;margin:0;padding:0">
{{< figure src="cert_aws_solutions_architect_professional.png" width="200" >}}
{{< figure src="cert_aws_solutions_architect_associate.png" width="200" >}}
{{< figure src="cert_aws_cloud_practitioner.png" width="200" >}}
{{< figure src="cert_aws_security_specialty.png" width="200" >}}
{{< figure src="cert_aws_data_analytics_specialty.png" width="200" >}}
{{< figure src="cert_aws_database_specialty.png" width="200" >}}
</section>

### Cloud Security Alliance

{{< figure alt="Cloud Security Alliance - CCSL V.4" src="cert_csa_ccsk_v4.png" width="200" >}}

### Linux Foundation

{{< figure src="cert_cka.png" alt="Certified Kubernetes Administrator" width="200" >}}
